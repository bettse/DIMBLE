
# DIMBLE

Disney Infinity Mobile Bluetooth Low Energy for use with the Apple TV

Icons from http://www.iconbeast.com/free/

## Features (short term)

### Base virtualization (piracy)

A fake BLE base + UI + token library to load up any token you want

### Base interface (cheating)

A 'client' (central) for the base that can read and write the values of real tokens placed on the base

### Proxy/MIRM (developing/debugging)

Connecting to the real base, while creating an emulated base, allowing viewing of all game<->base communication.

## Features (long term)

### Tag access key calculator

Scan a tag using the portal, or input a UID manually, and get the Mifare access key(s) for that tag.


## Design notes

Tabs for the three types of interactions.

Base Interface:
 * Color control
 * Read/parse tags that arrive
 * Dump tag to library
 * Modify tag and save

