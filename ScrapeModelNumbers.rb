#!/usr/bin/env ruby
# frozen_string_literal: true

require 'scrapify'

module DisneyInfinity
  # class
  class ModelNumber
    include Scrapify::Base
    html 'https://disneyinfinity.fandom.com/wiki/Model_Numbers'

    attribute :model, css: '#mw-content-text li'

    key :model

    def parts
      @parts ||= model.gsub(/[[:space:]]/, ' ').split('-', 3).map(&:strip)
    end

    def name
      parts[2]
    end

    def model_number
      parts[1]
    end

    def real?
      parts[0] == 'INF' && !name.empty?
    end

    def inspect
      "#<#{self.class.name} #{model_number} #{name}>"
    end

    def swift
      "#{model_number} : \"#{name}\","
    end

    def flipper
      "#{sprintf('%06x', model_number)}: #{name}"
    end
  end
end

def print_swift
  all = DisneyInfinity::ModelNumber.all()
  puts "    static let names : [Int:String] = ["

  all.each do |token|
    puts "        #{token.swift}" if token.real?
  end

  puts "    ]"
end

def print_flipper
  all = DisneyInfinity::ModelNumber.all
  puts 'Filetype: Flipper NFC resources'
  puts 'Version: 1'
  puts '# ID: Name'
  all.each do |token|
    puts token.flipper if token.real?
  end
end

print_flipper
