//
//  FakeBaseViewController.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/19/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import UIKit

class FakeBaseViewController : UIViewController {
    
    @IBOutlet weak var hexButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var onSwitch: UISwitch!
    @IBOutlet weak var magicBandButton: UIButton!
    
    let fakeBase = FakeBase.singleton
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        onSwitch.addTarget(self, action: #selector(FakeBaseViewController.firmwareSwitch), forControlEvents: .ValueChanged)
        magicBandButton.addTarget(self, action: #selector(FakeBaseViewController.addMagicBand), forControlEvents: .TouchUpInside)
        
        for button in buttonsForPlatform(.All) {
            button.layer.backgroundColor = UIColor.grayColor().CGColor
        }

        fakeBase.registerColorCallback(updateColor)
    }
    
    func updateColor(command: Command) {
        if let command = command as? LightCommand {
            NSLog("\(command)")
            for (ledPlatform, animation) in command.animations {
                for button in buttonsForPlatform(ledPlatform) { //In practice, this will be an array of one
                    animation.fromValue = button.layer.backgroundColor
                    button.layer.addAnimation(animation, forKey: "")
                }
            }
        }
    }
    
    func buttonsForPlatform(ledPlatform: LedPlatform) -> [UIButton] {
        switch ledPlatform {
        case .None:
            return []
        case .Left:
            return [leftButton]
        case .Right:
            return [rightButton]
        case .Hex:
            return [hexButton]
        case .All:
            return [leftButton, rightButton, hexButton]
        }
    }
    
    func firmwareSwitch() {
        //Disable idle timer when fakebase is on
        UIApplication.sharedApplication().idleTimerDisabled = onSwitch.on
        if onSwitch.on {
            fakeBase.start()
        } else {
            fakeBase.stop()
            for button in buttonsForPlatform(.All) {
                button.layer.removeAllAnimations()
                button.layer.backgroundColor = UIColor.grayColor().CGColor
            }
        }
        if let navController = self.parentViewController as? UINavigationController {
            if let tabBarViewController = navController.parentViewController as? UITabBarController {
                if let items = tabBarViewController.tabBar.items {
                    for item in items {
                        item.enabled = !onSwitch.on
                    }
                }
            }
        }
    }
    
    func addMagicBand() {
        fakeBase.addMagicBand()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if let platformViewController = segue.destinationViewController as? PlatformViewController {
            if (segue.identifier == "editLeftPlatform") {
                platformViewController.platform = .Left
            } else if (segue.identifier == "editHexPlatform") {
                platformViewController.platform = .Hex
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
}
