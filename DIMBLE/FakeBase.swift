//
//  FakeBase.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/28/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation


//This represents the physical base device.  It knows the tokens that are sitting on it, where they aren't sitting

class FakeBase {
    typealias colorCallback = (Command) -> Void
    static let singleton = FakeBase()

    var colorCallbacks : [colorCallback] = []
    var activeTokens : [LedPlatform : Set<EncryptedToken>]
    
    lazy var fakeBaseFirmware : FakeBaseFirmware = {
        [unowned self] in
        return FakeBaseFirmware(fakeBase: self)
    }()

    subscript(platform: LedPlatform) -> Set<EncryptedToken> {
        if let tokens = activeTokens[platform] {
            return tokens
        }
        return []
    }
    
    subscript(token: EncryptedToken) -> LedPlatform {
        for (platform, tokens) in activeTokens {
            for aToken in tokens {
                if aToken.uid == token.uid {
                    return platform
                }
            }
        }
        print("ERROR: unable to find token \(token) in \(activeTokens)")
        return LedPlatform.None
    }
    
    init() {
        activeTokens = [:]
    }

    func start() {
        fakeBaseFirmware.start()
    }
    
    func stop() {
        fakeBaseFirmware.stop()
    }
    
    //Consider replacing these with subscript set notation
    func addToken(ledPlatform: LedPlatform, token: EncryptedToken) {
        activeTokens[ledPlatform] = activeTokens[ledPlatform] ?? []
        activeTokens[ledPlatform]?.insert(token)
        fakeBaseFirmware.addToken(token, ledPlatform: ledPlatform)
    }
    
    func removeToken(ledPlatform: LedPlatform, token: EncryptedToken) {
        activeTokens[ledPlatform] = activeTokens[ledPlatform] ?? []
        activeTokens[ledPlatform]?.remove(token)
        fakeBaseFirmware.removeToken(token, ledPlatform: ledPlatform)
    }

    func addMagicBand() {
        let ledPlatform = LedPlatform.Right
        let magicBandToken = EncryptedToken(uid: NSData(fromHex: "04 EA DE AD BE EF 80"))
        magicBandToken.data = NSData(bytes: [UInt8](count: MifareMini.tokenSize, repeatedValue: 0)).mutableCopy() as! NSMutableData
        magicBandToken.nfcType = .DesFire
        fakeBaseFirmware.addToken(magicBandToken, ledPlatform: ledPlatform)
    }
    
    func registerColorCallback(callback: colorCallback) {
        self.colorCallbacks.append(callback)
    }
    
    func updateColor(command: Command) {
        dispatch_async(dispatch_get_main_queue(), {
            for callback in self.colorCallbacks {
                callback(command)
            }
        })
        
    }
}