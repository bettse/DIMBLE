//
//  Response.swift
//  DIMP
//
//  Created by Eric Betts on 6/19/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
import CryptoSwift //For chunks() extension

class Response : Message {
    let corrolationIdIndex = 0
    var corrolationId : UInt8 = 0
    let paramsIndex = 1
    var params : NSData

    //lol delegate
    var type : CommandType {
        get {
            return command.type
        }
    }
    
    var command : Command {
        get {
            return (Message.archive[corrolationId] as! Command)
        }
    }
    
    init(data: NSData) {
        data.getBytes(&corrolationId, range: NSMakeRange(corrolationIdIndex, sizeof(UInt8)))
        params = data.subdataWithRange(NSMakeRange(paramsIndex, data.length - paramsIndex))
        super.init()
    }

    convenience init(corrolationId: UInt8) {
        self.init(corrolationId: corrolationId, params: NSData())
    }
    
    init(corrolationId: UInt8, params: NSData) {
        self.corrolationId = corrolationId
        self.params = params
        super.init()
    }
    
    static func parse(data: NSData) -> Response {
        let r : Response = Response(data: data)
        switch r.command.type {
        case .Activate:
            return ActivateResponse(data: data)
        case .TagId:
            return TagIdResponse(data: data)
        case .B5:
            return B5Response(data: data)
        case .Presence:
            return PresenceResponse(data: data)
        case .Read:
            return ReadResponse(data: data)
        case .Write:
            return WriteResponse(data: data)
        case .LightOn:
            return LightOnResponse(data: data)
        case .LightFade:
            return LightFadeResponse(data: data)
        case .LightFlash:
            return LightFlashResponse(data: data)
        case .LightFlashAll:
            return LightFlashAllResponse(data: data)
        case .LightFadeAll:
            return LightFadeAllResponse(data: data)
        case .Seed:
            return SeedResponse(data: data)
        case .Challenge:
            return ChallengeResponse(data: data)
        }
    }
    
    override func serialize() -> NSData {
        let data = NSMutableData()
        data.appendBytes(&corrolationId, length: sizeof(UInt8))
        data.appendData(params)
        return data
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)()"
    }
}

class ActivateResponse : Response {
    override init(data: NSData) {
        super.init(data: data)
    }
    
    //from the BLE portal I got this ack:
    //00 4d 03 07 05 00 16 37 20 34 31 32 57 48 51 19 00 4f 00 2b
    init(corrolationId: UInt8) {
        super.init(corrolationId: corrolationId, params: NSData(fromHex: "00 0f 01 00 03 02 09 09 43 17 53 27 39 37 4e 42 48 f5 52 60"))
    }
}

class TagIdResponse : Response {
    let statusIndex = 0
    let tagIdIndex = 1
    var status : UInt8 = 0
    var tagId : NSData = NSData()
    
    override init(data: NSData) {
        super.init(data: data)
        status = params[statusIndex]
        tagId = params.subdataWithRange(NSMakeRange(tagIdIndex, 7))
    }
    
    init(corrolationId: UInt8, uid: NSData) {
        self.tagId = uid
        let temp = NSMutableData(bytes: [status])
        temp.appendData(uid)
        super.init(corrolationId: corrolationId, params: temp)
    }

    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(UID \(tagId))"
    }
}


class PresenceResponse : Response {
    var locations : [Location] = []
    
    override init(data: NSData) {
        super.init(data: data)
        let rawLocationPairs = params.arrayOfBytes()
        
        for i in 0.stride(to: rawLocationPairs.count, by: 2) {
            locations.append(Location(encodedLocation: rawLocationPairs[i+0], rawNfcType: rawLocationPairs[i+1]))
        }
    }
    
    init(corrolationId: UInt8, locations: [Location]) {
        self.locations = locations
        let encodedLocations = locations.flatMap({ (location) -> [UInt8] in return location.serialize() })
        super.init(corrolationId: corrolationId, params: NSData(bytes: encodedLocations))
    }

    //Empty presence
    convenience init(corrolationId: UInt8) {
        self.init(corrolationId: corrolationId, locations: [Location]())
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(\(locations))"
    }
    
    func asHex(value : UInt8) -> String {
        return "0x\(String(value, radix:0x10))"
    }
    
}

class ReadResponse : Response {
    let statusIndex = 0
    let blockDataIndex = 1

    
    //Delegates for easier access
    var blockNumber : UInt8  {
        get {
            if let command = command as? ReadCommand {
                return command.blockNumber
            }
            return 0
        }
    }
    var sector : UInt8  {
        get {
            if let command = command as? ReadCommand {
                return command.sector
            }
            return 0
        }
    }

    var blockData : NSData = NSData()
    var status : UInt8 = 0
    
    override init(data: NSData) {
        super.init(data: data)
        status = params[statusIndex]
        blockData = params.subdataWithRange(NSMakeRange(blockDataIndex, MifareMini.blockSize))
    }
    
    init(corrolationId: UInt8, status: UInt8, blockData: NSData) {
        self.blockData = blockData
        self.status = status
        let temp = NSMutableData(bytes: [status])
        temp.appendData(blockData)
        super.init(corrolationId: corrolationId, params: temp)
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(\(sector):\(blockNumber) => \(blockData))"
    }
}

class WriteResponse : Response {
    var status : UInt8 = 0
    let statusIndex = 0
    
    override init(data: NSData) {
        super.init(data: data)
        status = params[statusIndex]
    }
    
    init(corrolationId: UInt8) {
        let temp = NSMutableData(bytes: [status])
        super.init(corrolationId: corrolationId, params: temp)
    }

}

class ChallengeResponse : Response {
    let scrambledIndex = 1
    var value : UInt32 = 0
    
    override init(data: NSData) {
        var scrambled : UInt64 = 0
        data.getBytes(&scrambled, range: NSMakeRange(scrambledIndex, sizeof(scrambled.dynamicType)))
        super.init(data: data)
        value = descramble(scrambled.bigEndian)
    }
    
    init(corrolationId: UInt8, value: UInt32) {
        super.init(corrolationId: corrolationId, params: NSData())
        self.value = value
        var scrambledValue : UInt64 = scramble(value).bigEndian
        let scrambledData = NSMutableData(capacity: sizeof(UInt64))!
        scrambledData.replaceBytesInRange(NSMakeRange(0, sizeof(UInt64)), withBytes: &scrambledValue)
        params = NSData(data: scrambledData)
    }
    
    func descramble(input: UInt64) -> UInt32 {
        var scrambled : UInt64 = input
        var result : UInt64 = 0
        var mask : UInt64 = 0x5517999cd855aa71
    
        for _ in 1...64 {
            if ((mask & 1) == 1) {
                result = result << 1
                result |= (scrambled & 1)
            }
            scrambled = scrambled >> 1
            mask = mask >> 1
        }
        return UInt32(result)
    }
    
    func scramble(input: UInt32) -> UInt64 {
        var clear : UInt64 = UInt64(input)
        var result : UInt64 = 0
        var mask : UInt64 = 0x8E55AA1B3999E8AA
        
        for _ in 1...64 {
            result = result << 1
            if ((mask & 1) == 1) {
                result |= (clear & 1)
                clear = clear >> 1
            }
            mask = mask >> 1
        }
        return result
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me): 0x\(String(value, radix:0x10))"
    }
}

//Simple Ack
class B5Response : Response {}
class SeedResponse : Response {}
class LightOnResponse : Response {}
class LightFadeResponse : Response {}
class LightFlashResponse : Response {}
class LightFadeAllResponse : Response {}
class LightFlashAllResponse : Response {}



