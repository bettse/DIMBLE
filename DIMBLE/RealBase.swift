//
//  RealBase.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/13/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
import CoreBluetooth



class RealBase : NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    typealias incomingReport = (Report) -> Void
    typealias baseFound = () -> Void
    
    static let service_uuid = CBUUID(string: "6E400001-B5A3-F393-E0A9-E50E24DCCA9E")
    
    var centralManager : CBCentralManager?
    var bleBase : CBPeripheral?
    var readCharacteristic : CBCharacteristic?
    var writeCharacteristic : CBCharacteristic?
    var previousValue : NSMutableData?
    var incomingReportCallbacks : [incomingReport] = []
    var baseFoundCallbacks : [baseFound] = []

    func discover() {
        let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
        self.centralManager = CBCentralManager(delegate: self, queue: queue)
    }
    
    func registerIncomingReportCallback(callback: incomingReport) {
        incomingReportCallbacks.append(callback)
    }
    
    func registerBaseFoundCallback(callback: baseFound) {
        baseFoundCallbacks.append(callback)
    }
    
    func centralManagerDidUpdateState(central: CBCentralManager) {
        switch central.state {
        case .PoweredOff:
            print("BLE OFF")
        case .PoweredOn:
            let peripherals = central.retrieveConnectedPeripheralsWithServices([RealBase.service_uuid])
            if (peripherals.isEmpty) {
                print("No peripherals known, scanning")
                central.scanForPeripheralsWithServices([RealBase.service_uuid], options:nil)
            } else {
                if (peripherals.count > 1) {
                    print("Found \(peripherals.count) peripherals, but coded to only handle 1")
                }
                if let peripheral = peripherals.first {
                    if (self.bleBase == nil && peripheral.state == .Disconnected) {
                        self.bleBase = peripheral
                        central.connectPeripheral(peripheral, options: nil)
                    }
                }
            }
        case .Unknown:
            print("NOT RECOGNIZED")
        case .Unsupported:
            print("BLE NOT SUPPORTED")
        case .Resetting:
            print("BLE NOT SUPPORTED")
        default:
            print("Error")
        }
    }
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        print("didConnectPeripheral")
        central.stopScan()
        peripheral.delegate = self
        peripheral.discoverServices([RealBase.service_uuid])
    }
    
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        print("didDisconnectPeripheral")
    }
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        print("didDiscoverPeripheral \(peripheral)")
        if let bleBase = self.bleBase {
            if bleBase.state == .Disconnected {
                central.connectPeripheral(bleBase, options: nil)
            }
        } else if (peripheral.state == .Disconnected) {
            self.bleBase = peripheral
            central.connectPeripheral(peripheral, options: nil)
        }
    }
    
    func centralManager(central: CBCentralManager, willRestoreState peripheral: CBPeripheral, error: NSError?) {
        print("willRestoreState")
    }
    
    func centralManager(central: CBCentralManager, willRestoreState dict: [String : AnyObject]) {
        print("willRestoreState")
    }

    func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        print("didFailToConnectPeripheral [error: \(error)]")
    }
    
    //Mark - 
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        print("didDiscoverCharacteristicsForService [error: \(error)]")
        if let characteristics = service.characteristics {
            for characteristic in characteristics {
                if (characteristic.properties.contains(.Notify)) {
                    peripheral.setNotifyValue(true, forCharacteristic: characteristic)
                    readCharacteristic = characteristic
                }
                if (characteristic.properties.contains(.Write)) {
                    writeCharacteristic = characteristic
                }
            }
            if readCharacteristic != nil && writeCharacteristic != nil {
                for callback in baseFoundCallbacks {
                    callback()
                }
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverIncludedServicesForService service: CBService, error: NSError?) {
        print("didDiscoverIncludedServicesForService [error: \(error)]")
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        print("didDiscoverServices [error: \(error)]")
        if let services = peripheral.services {
            for service in services {
                peripheral.discoverCharacteristics(nil, forService: service)
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        print("didUpdateNotificationStateForCharacteristic [error: \(error)]")
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        if let error = error {
            print("didUpdateValueForCharacteristic [error: \(error)]")            
        } else if let newValue = characteristic.value {
            var completeMessage = newValue
            if let previousValue = self.previousValue {
                previousValue.appendData(newValue)
                completeMessage = previousValue
                self.previousValue = nil
            }
            
            if let report = Report(data: completeMessage) {
                //NSLog("RealBase => \(completeMessage)")
                for callback in incomingReportCallbacks {
                    callback(report)
                }
            } else {
                self.previousValue = (newValue.mutableCopy() as! NSMutableData)
            }
        }
    }
    
    func outgoingReport(report: Report) {
        if let peripheral = self.bleBase {
            if let writeCharacteristic = self.writeCharacteristic {
                //NSLog("RealBase <= \(report.serialize())")
                let newValue = report.serialize()
                if (newValue.length > 20) {
                    let part1 = newValue.subdataWithRange(NSMakeRange(0, 20))
                    let part2 = newValue.subdataWithRange(NSMakeRange(20, newValue.length - part1.length))
                    peripheral.writeValue(part1, forCharacteristic: writeCharacteristic, type: .WithResponse)
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        peripheral.writeValue(part2, forCharacteristic: writeCharacteristic, type: .WithResponse)
                    }
                } else {
                    peripheral.writeValue(newValue, forCharacteristic: writeCharacteristic, type: .WithResponse)
                }

            }
        }
    }

    func peripheral(peripheral: CBPeripheral, didWriteValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        if let error = error {
            print("didWriteValueForCharacteristic [error: \(error)]")
        }
    }
    
    func peripheralDidUpdateName(peripheral: CBPeripheral) {
        print("peripheralDidUpdateName")
    }
    
    func peripheralDidUpdateRSSI(peripheral: CBPeripheral, error: NSError?) {
        print("peripheralDidUpdateRSSI")
    }
    
    func peripheral(peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        print("didModifyServices")
    }
    
    func peripheral(peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: NSError?) {
        print("didReadRSSI [error: \(error)]")
    }
    
}