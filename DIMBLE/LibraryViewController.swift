//
//  libraryViewController.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/20/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import UIKit

class LibraryViewController: UITableViewController {
    @IBOutlet weak var generationSelection : UISegmentedControl!
    @IBOutlet weak var tokenTable : UITableView!
    
    var platform : LedPlatform?
    let fakeBase = FakeBase.singleton
    let theLibrary = TheLibrary.singleton
    var items : [EncryptedToken] = []

    
    //TODO: Filter library based on predicate (only hex, only characters, only gen X)
    //TODO: Include filter options in navigation bar
    override func viewDidLoad() {
        super.viewDidLoad()
        if let platform = self.platform {
            items = theLibrary.forPlatform(platform)
        }

        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.generationSelection.addTarget(self, action: #selector(LibraryViewController.filterGeneration), forControlEvents: .ValueChanged)
    }
    
    func filterGeneration() {
        let index = self.generationSelection.selectedSegmentIndex
        if let platform = self.platform {
            let all = theLibrary.forPlatform(platform)
            switch(index) {
            case 0: //All
                items = all
            default:
                items = all.filter({ (token) -> Bool in
                    token.decryptedToken.generation == UInt8(index)
                })
            }
        }
        tokenTable.reloadData()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        let token = self.items[indexPath.row].decryptedToken
        cell.textLabel?.text = token.shortDisplay
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let encryptedToken = self.items[indexPath.row]

        if let platform = self.platform {
            fakeBase.addToken(platform, token: encryptedToken)
        }

        navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)        
    }
}