//
//  TokenEditorViewController.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/22/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import UIKit

class TokenEditorViewController: UIViewController {
    @IBOutlet weak var onSwitch: UISwitch!
    
    let tokenEditorFirmware = TokenEditorFirmware.singleton
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        onSwitch.addTarget(self, action: #selector(TokenEditorViewController.firmwareSwitch), forControlEvents: .ValueChanged)
    }
    
    func firmwareSwitch() {
        UIApplication.sharedApplication().idleTimerDisabled = onSwitch.on
        if onSwitch.on {
            tokenEditorFirmware.start()
        } else {
            //TODO:
        }
        if let tabBarViewController = self.parentViewController as? UITabBarController {
            if let items = tabBarViewController.tabBar.items {
                for item in items {
                    item.enabled = !onSwitch.on
                }
            }
        }

    }
    

}
