//
//  LightCommand.swift
//  DIMBLE
//
//  Created by Eric Betts on 12/1/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
import UIKit
import UIColor_Hex_Swift

class LightCommand : Command {
    var animations : [LedPlatform : CABasicAnimation] {
        return [:]
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)()"
    }
}

class LightOnCommand : LightCommand {
    var red : UInt8 = 0, green: UInt8 = 0, blue : UInt8 = 0
    var ledPlatform : LedPlatform = .None
    
    override var animations : [LedPlatform:CABasicAnimation] {
        get {
            let pulseAnimation = CABasicAnimation(keyPath: "backgroundColor")
            //pulseAnimation.fromValue = //Set by receiving before using animation
            pulseAnimation.toValue = color.CGColor
            pulseAnimation.duration = 0.001
            pulseAnimation.fillMode = kCAFillModeForwards
            pulseAnimation.removedOnCompletion = false
            return [ledPlatform : pulseAnimation]
        }
    }
    
    var color : UIColor {
        get {
            let max = CGFloat(UInt8.max)
            let r = CGFloat(red)/max
            let g = CGFloat(green)/max
            let b = CGFloat(blue)/max
            return UIColor(red: r, green: g, blue: b, alpha: 1.0)
        }
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(\(ledPlatform): \(color.hexString(false)))"
    }
    
    init(ledPlatform: LedPlatform, red : UInt8, green: UInt8, blue : UInt8) {
        super.init()
        self.ledPlatform = ledPlatform
        self.red = red
        self.green = green
        self.blue = blue
        type = .LightOn
        params = NSData(bytes: [ledPlatform.rawValue, red, green, blue] as [UInt8], length: 4)
    }
    
    convenience init(ledPlatform: LedPlatform, red : Int, green: Int, blue : Int) {
        self.init(ledPlatform: ledPlatform, red: UInt8(red), green: UInt8(green), blue: UInt8(blue))
    }
    
    convenience init(ledPlatform: LedPlatform, color : UIColor) {
        var r : UInt8 = 0, g: UInt8 = 0, b : UInt8 = 0
        let scale : CGFloat = CGFloat(UInt8.max)
        var red : CGFloat = 0 , green : CGFloat = 0 , blue : CGFloat = 0 , alpha : CGFloat = 0
        
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        r = UInt8(Int(red * scale))
        g = UInt8(Int(green * scale))
        b = UInt8(Int(blue * scale))
        
        self.init(ledPlatform: ledPlatform, red: r, green: g, blue: b)
    }
    
    override init(data: NSData) {
        super.init(data: data)
        if let ledPlatform = LedPlatform(rawValue: params[0]) {
            self.ledPlatform = ledPlatform
        } else {
            print("Error determining ledPlatform for value \(data[0])")
        }
        red = params[1]
        green = params[2]
        blue = params[3]
    }
}

class LightFadeCommand : LightCommand {
    var fade : Fade = Fade(data: NSData(fromHex: "00 00 00 00 00"))
    var ledPlatform : LedPlatform = .None
    
    override var animations : [LedPlatform : CABasicAnimation] {
        return [ledPlatform : fade.animation]
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(\(ledPlatform) : \(fade))"
    }
    
    //TODO: Add an init that takes a Fade object, or that takes components to make a Fade object
    
    override init(data: NSData) {
        super.init(data: data)
        if let ledPlatform = LedPlatform(rawValue: params[0]) {
            self.ledPlatform = ledPlatform
        }
        fade = Fade(data: params.subdataWithRange(NSMakeRange(1, 5)))
    }
}

class LightFadeAllCommand : LightCommand {
    var HFade : Fade = Fade(data: NSData(fromHex: "00 00 00 00 00"))
    var LFade : Fade = Fade(data: NSData(fromHex: "00 00 00 00 00"))
    var RFade : Fade = Fade(data: NSData(fromHex: "00 00 00 00 00"))

    override var animations : [LedPlatform:CABasicAnimation] {
        return [:]
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(Hex: \(HFade) | Left: \(LFade) | Right: \(RFade))"
    }
    
    override init(data: NSData) {
        super.init(data: data)
        //01000000 01000000 01000000
        print("LightFadeAll: \(params)")
    }
}

class LightFlashCommand : LightCommand {
    var flash : Flash = Flash(data: NSData(fromHex: "00 00 00 00 00 00"))
    var ledPlatform : LedPlatform = .None
    
    override var animations : [LedPlatform:CABasicAnimation] {
        return [ledPlatform : flash.animation]
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(\(ledPlatform) : \(flash))"
    }
    
    override init(data: NSData) {
        super.init(data: data)
        if let ledPlatform = LedPlatform(rawValue: params[0]) {
            self.ledPlatform = ledPlatform
        }
        flash = Flash(data: params.subdataWithRange(NSMakeRange(1, 6)))
    }
}

class LightFlashAllCommand : LightCommand {
    var HFlash : Flash = Flash(data: NSData(fromHex: "00 00 00 00 00 00"))
    var LFlash : Flash = Flash(data: NSData(fromHex: "00 00 00 00 00 00"))
    var RFlash : Flash = Flash(data: NSData(fromHex: "00 00 00 00 00 00"))
    
    override var animations : [LedPlatform:CABasicAnimation] {
        return [.Hex : HFlash.animation, .Left: LFlash.animation, .Right: RFlash.animation]
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(Hex: \(HFlash) | Left: \(LFlash) | Right: \(RFlash))"
    }
    
    override init(data: NSData) {
        super.init(data: data)

        //010a02faf080 010a02faf07d 010a02faf080

        HFlash = Flash(data: params.subdataWithRange(NSMakeRange(0, 6)))
        LFlash = Flash(data: params.subdataWithRange(NSMakeRange(6, 6)))
        RFlash = Flash(data: params.subdataWithRange(NSMakeRange(12, 6)))
    }
}
