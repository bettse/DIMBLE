//
//  BurtleRandom.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/17/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
class BurtleRandom {
    var a : UInt32 = 0xf1ea5eed
    var b : UInt32 = 0
    var c : UInt32 = 0
    var d : UInt32 = 0
    let startingIterations = 23
    
    init(seed: UInt32) {
        b = seed
        c = seed
        d = seed
        for _ in 1...startingIterations {
            self.value()
        }
    }
    
    convenience init(seed: Int) {
        let s : UInt32 = UInt32(seed)
        self.init(seed: s)
    }
    
    func rot(x: UInt32, k: UInt32) -> UInt32 {
        return UInt32(
            ( x << k ) | ( x >> (32 - k) )
        )
    }
    
    func value() -> UInt32 {
        //&+ / &- are swift's overflow operators.  By default, overflows are an error
        let e : UInt32 = a &- rot(b, k: 27)
        a = b ^ rot(c, k: 17)
        b = c &+ d
        c = d &+ e
        d = e &+ a
        return d
    }
}
