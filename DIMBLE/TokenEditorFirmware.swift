//
//  TokenEditorFirmware.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/16/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation

class TokenEditorFirmware {
    let base = RealBase()
    static let singleton = TokenEditorFirmware()
    
    init() {
        base.registerBaseFoundCallback(baseFound)
        base.registerIncomingReportCallback(incomingReport)
    }
    
    func start() {
        base.discover()
    }
    
    func baseFound() {
        print("Discovered a base!")
        let activate = ActivateCommand()
        base.outgoingReport(Report(cmd: activate))
    }
    
    func incomingReport(report: Report) {
        if let response = report.content as? Response {
            incomingResponse(response)
        } else if let update = report.content as? Update {
            incomingUpdate(update)
        }
    }
    
    func incomingResponse(response : Response) {

        switch(response.type) {
        case .TagId:
            if let response = response as? TagIdResponse {
                print("TagId: \(response.tagId.description.componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString:"< >")).joinWithSeparator(""))")
            }
        default:
            break
            //print("Response: \(response)")
        }
    }
    
    func incomingUpdate(update : Update) {
        //print("Update: \(update)")
        if update.direction == .Arriving {
            base.outgoingReport(Report(cmd: TagIdCommand(nfcIndex: update.nfcIndex)))
        }

    }
}