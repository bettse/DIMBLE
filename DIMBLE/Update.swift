//
//  Update.swift
//  DIMP
//
//  Created by Eric Betts on 6/19/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation

enum Direction : UInt8 {
    case Arriving = 0
    case Departing = 1
    func desc() -> String {
        return String(self).componentsSeparatedByString(".").last!
    }
}

//Sorry about having something call nfcIndexIndex; it was the blending of the two patterns
class Update : Message {
    let ledPlatformIndex = 0
    let nfcTypeIndex = 1
    let nfcIndexIndex = 2
    let directionIndex = 3
    
    //Setting defaults so I don't have to deal with '?' style variables yet
    var ledPlatform : LedPlatform = .None
    var nfcIndex : UInt8 = 0
    var direction : Direction = .Arriving
    var nfcType : NfcType = .MifareMini
    
    init(data: NSData) {
        data.getBytes(&ledPlatform, range: NSMakeRange(ledPlatformIndex, sizeof(LedPlatform)))
        data.getBytes(&nfcType, range: NSMakeRange(nfcTypeIndex, sizeof(NfcType)))
        data.getBytes(&nfcIndex, range: NSMakeRange(nfcIndexIndex, sizeof(nfcIndex.dynamicType)))
        data.getBytes(&direction, range: NSMakeRange(directionIndex, sizeof(Direction)))
    }
    
    init(location: Location, direction: Direction) {
        self.ledPlatform = location.ledPlatform
        self.nfcType = location.nfcType
        self.nfcIndex = location.nfcIndex
        self.direction = direction
    }
    
    //This is pretty silly, but creates symmetry with Response and Command
    static func parse(data: NSData) -> Update {
        return Update(data: data)
    }
    
    override func serialize() -> NSData {
        return NSData(bytes: [ledPlatform.rawValue, nfcType.rawValue, nfcIndex, direction.rawValue] as [UInt8], length: 4)
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(#\(nfcIndex) \(nfcType) \(direction) on \(ledPlatform) platform)"
    }
}