//
//  BaseAnimation.swift
//  DIMBLE
//
//  Created by Eric Betts on 12/1/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
import UIKit
import UIColor_Hex_Swift

class BaseAnimation : CustomStringConvertible {
    let secondsPerByte = 8.0 //experimental, rough
    var red : UInt8 = 0
    var green : UInt8 = 0
    var blue : UInt8 = 0
    
    var color : UIColor {
        get {
            let max = CGFloat(UInt8.max)
            let r = CGFloat(red)/max
            let g = CGFloat(green)/max
            let b = CGFloat(blue)/max
            return UIColor(red: r, green: g, blue: b, alpha: 1.0)
        }
    }
    
    var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(\(color))"
    }
}


class Fade : BaseAnimation {
    var duration: UInt8 = 1
    var count : UInt8 = 2 //Even to end on original color, odd to end on new color
    
    var animation : CABasicAnimation {
        get {
            let pulseAnimation = CABasicAnimation(keyPath: "backgroundColor")
            //pulseAnimation.fromValue = //Set by receiving before using animation
            pulseAnimation.toValue = color.CGColor
            pulseAnimation.duration = Double(duration/UInt8.max) * secondsPerByte
            pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            pulseAnimation.repeatCount = Float(count/2)
            pulseAnimation.autoreverses = (count % 2 == 0)
            if (count % 2 == 1) { //Remain on new color
                pulseAnimation.fillMode = kCAFillModeForwards
                pulseAnimation.removedOnCompletion = false
            }
            return pulseAnimation
        }
    }
    
    
    init(data: NSData) {
        if (data.length != 5) {
            print("Incorrect data length to Fade class")
        }
        super.init()
        
        duration = data[0]
        count = data[1]
        red = data[2]
        green = data[3]
        blue = data[4]
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(duration:\(duration) count:\(count) \(color.hexString(false)))"
    }
}

class Flash : BaseAnimation {
    var count : UInt8 = 1
    var timeNew : UInt8 = 0
    var timeOld : UInt8 = 0
    
    var animation : CABasicAnimation {
        get {
            let pulseAnimation = CABasicAnimation(keyPath: "backgroundColor")
            //pulseAnimation.fromValue = //Set by receiving before using animation
            pulseAnimation.toValue = color.CGColor
            pulseAnimation.duration = Double((timeNew + timeOld)/UInt8.max) * secondsPerByte
            pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            pulseAnimation.repeatCount = Float(count/2)
            pulseAnimation.autoreverses = (count % 2 == 0)
            if (count % 2 == 1) { //Remain on new color if odd
                pulseAnimation.fillMode = kCAFillModeForwards
                pulseAnimation.removedOnCompletion = false
            }
            return pulseAnimation
        }
    }
    
    init(data: NSData) {
        if (data.length != 6) {
            print("Incorrect data length to Flash class")
        }
        super.init()
        
        timeNew = data[0]
        timeOld = data[1]
        count = data[2]
        red = data[3]
        green = data[4]
        blue = data[5]
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(\(timeNew):\(timeOld)x\(count) \(color.hexString(false)))"
    }
}

