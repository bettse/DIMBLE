//
//  Command.swift
//  DIMP
//
//  Created by Eric Betts on 6/19/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
import UIKit

class Command : Message {
    let typeIndex = 0
    let corrolationIdIndex = 1
    let paramsIndex = 2

    static var corrolationGenerator = (1..<UInt8.max-1).generate()
    static var nextSequence : UInt8 {
        get {
            if let next = corrolationGenerator.next() {
                return next
            } else {
                corrolationGenerator = (1..<UInt8.max-1).generate()
                return 0
            }
        }
    }
    
    var type : CommandType = .Activate
    var corrolationId : UInt8 = 0
    var params : NSData = NSData()
    
    override init() {
        corrolationId = Command.nextSequence
        super.init()
        Message.archive[corrolationId] = self
    }
    
    //Parsing from NSData
    init(data: NSData) {
        if let type = CommandType(rawValue: data[typeIndex]) {
            self.type = type
        } else {
            print("Unhandled command type \(data[typeIndex])")
        }
        corrolationId = data[corrolationIdIndex]
        params = data.subdataWithRange(NSMakeRange(paramsIndex, data.length - paramsIndex))
        super.init()
        Message.archive[corrolationId] = self
    }
    
    static func parse(data: NSData) -> Command {
        //print("Command::Parse data=\(data)")
        let c : Command = Command(data: data)
        switch c.type {
        case .Activate:
            return ActivateCommand(data: data)
        case .Seed:
            return SeedCommand(data: data)
        case .Challenge:
            return ChallengeCommand(data: data)
        case .LightOn:
            return LightOnCommand(data: data)
        case .LightFade:
            return LightFadeCommand(data: data)
        case .LightFlash:
            return LightFlashCommand(data: data)
        case .LightFlashAll:
            return LightFlashAllCommand(data: data)
        case .LightFadeAll:
            return LightFadeAllCommand(data: data)
        case .Presence:
            return PresenceCommand(data: data)
        case .Read:
            return ReadCommand(data: data)
        case .Write:
            return WriteCommand(data: data)
        case .TagId:
            return TagIdCommand(data: data)
        case .B5:
            return B5Command(data: data)
        }
    }
    
    
    func response() -> Response {
        return Response(corrolationId: corrolationId)
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)()"
    }
    
    override func serialize() -> NSData {
        let data = NSMutableData()
        var rawType : UInt8 = type.rawValue
        data.appendBytes(&rawType, length: sizeof(UInt8))
        data.appendBytes(&corrolationId, length: sizeof(UInt8))
        data.appendData(params)
        return data
    }
}

class ActivateCommand : Command {
    override init() {
        super.init()
        type = .Activate
        params = DisneyInfinity.magic
    }
    
    override init(data: NSData) {
        super.init(data: data)
    }
    
    override func response() -> Response {
        return ActivateResponse(corrolationId: corrolationId)
    }
}

class SeedCommand : Command {
    var value : UInt32 = 0
    
    override init() {
        super.init()
        type = .Seed
        params = NSData(bytes: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00] as [UInt8], length: 8)
    }
    
    override init(data: NSData) {
        super.init(data: data)
        var scrambled : UInt64 = 0
        params.getBytes(&scrambled, length: sizeof(UInt64))
        value = descramble(scrambled.bigEndian)
    }
    
    func descramble(input: UInt64) -> UInt32 {
        var scrambled : UInt64 = input
        var result : UInt64 = 0
        var mask : UInt64 = 0x5517999cd855aa71
        
        for _ in 0..<64 {
            if ((mask & 1) == 1) {
                result = result << 1;
                result |= (scrambled & 1);
            }
            scrambled = scrambled >> 1;
            mask = mask >> 1;
        }
        return UInt32(result);
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(\(value))"
    }

}

class ChallengeCommand : Command {
    override init() {
        super.init()
        type = .Challenge
    }
    
    override init(data: NSData) {
        super.init(data: data)
    }
    
    func response(value : UInt32) -> Response {
        return ChallengeResponse(corrolationId: corrolationId, value: value)
    }
}

class PresenceCommand : Command {
    override init() {
        super.init()
        type = .Presence
    }
    override init(data: NSData) {
        super.init(data: data)
    }
    
    func response(locations : [Location]) -> Response {
        return PresenceResponse(corrolationId: corrolationId, locations: locations)
    }
}

class TagIdCommand : Command {
    var nfcIndex : UInt8 = 0
    init(nfcIndex: UInt8) {
        self.nfcIndex = nfcIndex
        super.init()
        type = .TagId
        params = NSData(bytes: [nfcIndex] as [UInt8], length: 1)
    }
    override init(data: NSData) {
        super.init(data: data)
        params.getBytes(&nfcIndex, range: NSMakeRange(0, sizeof(nfcIndex.dynamicType)))
    }
    
    func response(uid : NSData) -> Response {
        return TagIdResponse(corrolationId: corrolationId, uid: uid)
    }
}

class B5Command : Command {
    var value : UInt8 = 0
    override init() {
        super.init()
        type = .B5
    }
    override init(data: NSData) {
        super.init(data: data)
        value = params[0]
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(\(value))"
    }
}

class ReadCommand : Command {
    var nfcIndex : UInt8 = 0
    var blockNumber : UInt8 = 0
    var sector : UInt8 = 0

    init(nfcIndex: UInt8, block: UInt8) {
        self.nfcIndex = nfcIndex
        self.blockNumber = block
        super.init()
        type = .Read
        params = NSData(bytes: [nfcIndex, sector, block] as [UInt8], length: 3)
    }
    
    convenience init(nfcIndex: UInt8, block: Int) {
        self.init(nfcIndex: nfcIndex, block: UInt8(block))
    }
    
    override init(data: NSData) {
        super.init(data: data)
        nfcIndex = params[0]
        sector = params[1]
        blockNumber = params[2]
    }
    
    func response(blockData: NSData) -> Response {
        return ReadResponse(corrolationId: corrolationId, status: 0, blockData: blockData)
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(nfc #\(nfcIndex) \(sector):\(blockNumber))"
    }
}

class WriteCommand : Command {
    var nfcIndex : UInt8 = 0
    var sector : UInt8 = 0
    var blockNumber : UInt8 = 0
    var blockData : NSData = NSData()
    
    init(nfcIndex: UInt8, block: UInt8, blockData: NSData) {
        self.nfcIndex = nfcIndex
        self.blockNumber = block
        self.blockData = blockData
        super.init()
        type = .Write
        let temp : NSMutableData = NSMutableData(bytes: [nfcIndex, 0x00, block] as [UInt8], length: 3)
        temp.appendData(blockData)
        params = NSData(data: temp)
    }
    
    convenience init(nfcIndex: UInt8, block: Int, blockData: NSData) {
        self.init(nfcIndex: nfcIndex, block: UInt8(block), blockData: blockData)
    }
    
    override init(data: NSData) {
        super.init(data: data)
        params.getBytes(&nfcIndex, range: NSMakeRange(0, sizeof(nfcIndex.dynamicType)))
        params.getBytes(&sector, range: NSMakeRange(1, sizeof(sector.dynamicType)))
        params.getBytes(&blockNumber, range: NSMakeRange(2, sizeof(blockNumber.dynamicType)))
        blockData = params.subdataWithRange(NSMakeRange(3, MifareMini.blockSize))
    }
    
    override func response() -> Response {
        return WriteResponse(corrolationId: corrolationId)
    }
    
    override var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)(NFC #\(nfcIndex) \(sector):\(blockNumber) => \(blockData))"
    }
}

