//
//  ProxyViewController.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/21/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
import UIKit

class ProxyViewController: UIViewController {

    @IBOutlet weak var proxySwitch : UISwitch!
    @IBOutlet weak var log : UITextView!
    
    let font = UIFont(name: "CourierNewPSMT", size: 18.0)!
    let proxyBase = ProxyBase()
    let autoScroll = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        proxySwitch.addTarget(self, action: #selector(ProxyViewController.toggleProxy), forControlEvents: .ValueChanged)
        proxyBase.registerFromBaseCallback(fromBase)
        proxyBase.registerFromGameCallback(fromGame)
    }
    
    func toggleProxy() {
        UIApplication.sharedApplication().idleTimerDisabled = proxySwitch.on
        if proxySwitch.on {
            log.text = "[Starting proxy]"
            proxyBase.discoverRealBase()
        } else {
            //TODO: Stop proxy
        }
        if let tabBarViewController = self.parentViewController as? UITabBarController {
            if let items = tabBarViewController.tabBar.items {
                for item in items {
                    item.enabled = !proxySwitch.on
                }
            }
        }
    }
    
    func fromBase(report: Report) {
        dispatch_async(dispatch_get_main_queue(), {
            var newLine : NSAttributedString = NSAttributedString(string: "", attributes: nil)
            if let update = report.content as? Update {
                let attr = [
                    NSForegroundColorAttributeName: UIColor.brownColor(),
                    NSFontAttributeName: self.font
                ]
                newLine = NSAttributedString(string: update.description, attributes: attr)
                NSLog("Base => \(update)")
            } else if let response = report.content as? Response {
                let attr = [
                    NSForegroundColorAttributeName: UIColor.grayColor(),
                    NSFontAttributeName: self.font
                ]
                newLine = NSAttributedString(string: response.description, attributes: attr)
                NSLog("Base => \(response)")
            }
            
            let accumulator = self.log.attributedText.mutableCopy() as! NSMutableAttributedString
            accumulator.appendAttributedString(NSAttributedString(string: "\n", attributes: nil))
            accumulator.appendAttributedString(newLine)
            self.log.attributedText = NSAttributedString(attributedString: accumulator)
            
            //Scoll to bottom
            if (self.autoScroll) {
                let range = NSMakeRange(self.log.attributedText.length - 1, 1);
                self.log.scrollRangeToVisible(range)
                self.log.layoutIfNeeded()
            }
        })
    }
    
    func fromGame(report: Report) {
        dispatch_async(dispatch_get_main_queue(), {

            var newLine : NSAttributedString = NSAttributedString(string: "", attributes: nil)
            if let command = report.content as? Command {
                let attr = [
                    NSFontAttributeName: self.font
                ]
                newLine = NSAttributedString(string: command.description, attributes: attr)
                NSLog("Game => \(command)")
            }
            
            let accumulator = self.log.attributedText.mutableCopy() as! NSMutableAttributedString
            accumulator.appendAttributedString(NSAttributedString(string: "\n", attributes: nil))
            accumulator.appendAttributedString(newLine)
            self.log.attributedText = NSAttributedString(attributedString: accumulator)
            
            //Scoll to bottom
            if (self.autoScroll) {
                let range = NSMakeRange(self.log.attributedText.length - 1, 1);
                self.log.scrollRangeToVisible(range)
                self.log.layoutIfNeeded()
            }
        })
    }
    
}