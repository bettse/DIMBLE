//
//  DisneyInfinity.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/12/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation

class DisneyInfinity {
    static let magic : NSData = "(c) Disney 2013".dataUsingEncoding(NSASCIIStringEncoding)!
    static let secret : NSData = NSData(fromHex: "AF 62 D2 EC 04 91 96 8C C5 2A 1A 71 65 F8 65 FE")
}

enum NfcType : UInt8 {
    case None = 0
    case MifareMini = 0x09
    case DesFire = 0x20
}