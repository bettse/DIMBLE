//
//  FakeBaseFirmware.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/12/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
import UIKit
class FakeBaseFirmware {
    static let maxNFC = 0x10

    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let bleInterface = FakeBaseInterface()
    var burtle : BurtleRandom?
    var activeTokens : [UInt8 : EncryptedToken] = [:]
    var fakeBase : FakeBase
    
    var locations : [Location] {
        get {
            return activeTokens.map { (nfcIndex, token) -> Location in
                return Location(ledPlatform: fakeBase[token], nfcIndex: nfcIndex, nfcType: token.nfcType)
            }.sort({ (a, b) -> Bool in
                return a.nfcIndex < b.nfcIndex
            })
        }
    }
    
    init(fakeBase: FakeBase) {
        self.fakeBase = fakeBase
        bleInterface.registerIncomingReportCallback(incomingReport)
    }
    
    func start() {
        bleInterface.start()
    }
    
    func stop() {
        bleInterface.stop()
        reset()
    }
    
    func reset() {
        burtle = nil
    }
    
    //Replace with subscript assignment?
    func addToken(token: EncryptedToken, ledPlatform: LedPlatform) {
        var tokenIndex : UInt8 = 0
        for i in 0...activeTokens.count {
            tokenIndex = UInt8(i)
            if (activeTokens[tokenIndex] == nil) {
                activeTokens[tokenIndex] = token
                break
            }
        }
        let location = Location(ledPlatform: ledPlatform, nfcIndex: tokenIndex, nfcType: token.nfcType)
        let update = Update(location: location, direction: .Arriving)
        NSLog("FakeBaseFirmware => \(update)")
        bleInterface.outgoingReport(Report(update: update))
    }
    
    func removeToken(token: EncryptedToken, ledPlatform: LedPlatform) {
        var tokenIndex : UInt8 = 0
        for (index, aToken) in activeTokens {
            if aToken.uid == token.uid {
                tokenIndex = index
                break
            }
        }
        activeTokens.removeValueForKey(tokenIndex)
        let location = Location(ledPlatform: ledPlatform, nfcIndex: tokenIndex)
        let update = Update(location: location, direction: Direction.Departing)
        NSLog("FakeBaseFirmware => \(update)")
        bleInterface.outgoingReport(Report(update: update))
    }
        
    func incomingReport(report: Report) {
        //For emulating the base, the console will only ever send Commands
        if let command = report.content as? Command {
            incomingCommand(command)
        } else {
            print("Received a report \(report) with a content that wasn't a Command")
        }
    }
    
    func incomingCommand(cmd: Command) {
        var response : Response = cmd.response()
        if let command = cmd as? ActivateCommand {
            reset()
            response = command.response()
        } else if let command = cmd as? SeedCommand {
            if let _ = self.burtle {
                print("ERROR: self.burtle already defined")
            }
            self.burtle = BurtleRandom(seed: command.value)
        } else if let command = cmd as? ChallengeCommand {
            if let burtle = burtle {
                response = command.response(burtle.value())
            } else {
                print("ERROR: Burtle value requested, but burtle not defined")
            }
        } else if let command = cmd as? PresenceCommand {
            response = command.response(locations)
        } else if let command = cmd as? TagIdCommand {
            if let token = activeTokens[command.nfcIndex] {
                response = command.response(token.uid)
            } else {
                print("ERROR: Unable to get UID for token at nfc \(command.nfcIndex)")
            }
        } else if let command = cmd as? ReadCommand {
            if let token = activeTokens[command.nfcIndex] {
                let blockNumber = command.blockNumber + UInt8(MifareMini.sectorSize) * command.sector
                response = command.response(token.block(blockNumber))
            } else {
                print("ERROR: Unable to read token at nfc \(command.nfcIndex)")
                response = ReadResponse(corrolationId: command.corrolationId, status: 0x83, blockData: NSData(bytes: [UInt8](count: MifareMini.blockSize, repeatedValue: 0)))
            }
        } else if let command = cmd as? WriteCommand {
            response = command.response()
            if let token = activeTokens[command.nfcIndex] {
                let blockNumber = command.blockNumber + UInt8(MifareMini.sectorSize) * command.sector
                //Load the new data
                token.load(blockNumber, blockData: command.blockData)
                //Write to disk
                token.dump(appDelegate.applicationDocumentsDirectory)
            } else {
                print("ERROR: Unable to write token at nfc \(command.nfcIndex)")
            }
        } else if let command = cmd as? LightOnCommand {
            fakeBase.updateColor(command)
        } else if let command = cmd as? LightFlashCommand {
            fakeBase.updateColor(command)
        } else if let command = cmd as? LightFlashAllCommand {
            fakeBase.updateColor(command)
        } else if let command = cmd as? LightFadeCommand {
            fakeBase.updateColor(command)
        } else if let command = cmd as? LightFadeAllCommand {
            fakeBase.updateColor(command)
        }
        bleInterface.outgoingReport(Report(resp: response))
    }
}