//
//  platformViewController.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/19/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import UIKit

class PlatformViewController : UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var platformCollection: UICollectionView!
    
    let fakeBase = FakeBase.singleton
    var platform : LedPlatform?
    var tokens : [EncryptedToken] {
        get {
            if let platform = self.platform {
                return Array(fakeBase[platform])
            }
            print("PlatformViewController, platform not defined")
            return []
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let platform = self.platform {
            self.title = "\(platform)"
        }
        

        platformCollection.delegate = self
        platformCollection.dataSource = self
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tokens.count
    }

    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let token = tokens[indexPath.row]
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("tokenCell", forIndexPath: indexPath)
        let label = cell.viewWithTag(100) as! UILabel
        
        label.text = token.decryptedToken.shortDisplay
        cell.layer.borderWidth = 3.0
        cell.layer.borderColor = token.decryptedToken.model.color.CGColor

        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(PlatformViewController.respondToSwipeGesture(_:)))
        swipe.direction = UISwipeGestureRecognizerDirection.Left
        platformCollection.addGestureRecognizer(swipe)
        
        return cell
    }
    
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.Down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.Left:
                print("Swiped left")
                let p = gesture.locationInView(platformCollection)
                let indexPath = platformCollection.indexPathForItemAtPoint(p)
                if let indexPath = indexPath {
                    let token = tokens[indexPath.row]
                    if let platform = self.platform {
                        fakeBase.removeToken(platform, token: token)
                        platformCollection.reloadData()
                    }
                }
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        platformCollection.reloadData()
        super.viewWillAppear(animated);
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if let libraryViewController = segue.destinationViewController as? LibraryViewController {
            if (segue.identifier == "loadLibrary") {
                libraryViewController.platform = platform
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
