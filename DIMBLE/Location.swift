//
//  Location.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/19/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation

enum LedPlatform : UInt8 {
    case All = 0
    case Hex = 1
    case Left = 2
    case Right = 3
    case None = 0xFF
    func desc() -> String {
        return String(self).componentsSeparatedByString(".").last!
    }
}

class Location : CustomStringConvertible {
    var ledPlatform : LedPlatform
    var nfcIndex : UInt8
    var nfcType : NfcType
    
    init(encodedLocation: UInt8, rawNfcType: UInt8 = 0x09) {
        if let led = LedPlatform(rawValue: encodedLocation.high_nibble) {
            ledPlatform = led
        } else {
            ledPlatform = .None
        }
        if let nfcType = NfcType(rawValue: rawNfcType) {
            self.nfcType = nfcType
        } else {
            self.nfcType = .MifareMini
        }
        nfcIndex = encodedLocation.low_nibble
    }
    
    init(ledPlatform: LedPlatform, nfcIndex: UInt8, nfcType : NfcType = .MifareMini) {
        self.ledPlatform = ledPlatform
        self.nfcIndex = nfcIndex
        self.nfcType = nfcType
    }
    
    func serialize() -> [UInt8] {
        //high is led
        //low is nfc
        return [(ledPlatform.rawValue << 4) | nfcIndex, nfcType.rawValue]
    }
    
    var description: String {
        return "NFC #\(self.nfcIndex) \(nfcType) on \(self.ledPlatform) Platform"
    }
}