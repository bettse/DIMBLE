//
//  Report.swift
//  DIMP
//
//  Created by Eric Betts on 6/19/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation

//All data sent across USB is a 'Report'.  It always starts with a byte like 0xff/0xaa/0xab and ends with a summation checksum
//Sometimes the Reports are from device and need to be parsed, othertimes they're constructed and then need to be serialized



class Report : CustomStringConvertible {
    let typeIndex = 0
    let lengthIndex = 1
    let contentIndex = 2
    
    enum MessageType : UInt8 {
        case Command = 0xFF
        case Response = 0xAA
        case Update = 0xAB
        func desc() -> String {
            return String(self).componentsSeparatedByString(".").last!
        }
    }
    
    var type : MessageType
    var length = 0 //TODO: Convert to use getting/setter

    // (command, response, update)
    var content : Message = Message()

    var checksum : UInt8 {
        get {
            var sum = Int(type.rawValue)
            let b = UnsafeBufferPointer<UInt8>(start: UnsafePointer(content.serialize().bytes), count: length)
            for i in 0..<length {
                sum += Int(b[i])
            }            
            return UInt8((sum + length) & 0xff)
        }
        set(newChecksum) {
            //data.getBytes(&checksum, range: NSMakeRange(length, sizeof(UInt8)))
            print("I don't know how to set the checksum to \(newChecksum)")
        }
    }
    
    init?(data: NSData) {
        var temp : UInt8 = 0
        data.getBytes(&temp, range: NSMakeRange(typeIndex, sizeof(temp.dynamicType)))
        data.getBytes(&length, range: NSMakeRange(lengthIndex, sizeof(UInt8)))
        if let type = MessageType(rawValue: temp) {
            self.type = type
        } else {
            self.type = .Command
            return nil
        }
        let endOfData = contentIndex + length
        
        //'length' doens't include final checksum byte
        if (endOfData < data.length) {
            let rawContentData = data.subdataWithRange(NSMakeRange(contentIndex, length))
            switch type {
            case .Response:
                content = Response.parse(rawContentData)
            case .Update:
                content = Update.parse(rawContentData)
            case .Command:
                content = Command.parse(rawContentData)
            }
        } else {
            //print("Too little data provided to Report::init (has \(data.length - contentIndex), needs \(length))")
            return nil
        }
        
        //Final byte is checksum
        var chksum : UInt8 = 0
        data.getBytes(&chksum, range: NSMakeRange(endOfData, sizeof(UInt8)))
        if (chksum != self.checksum) {
            print("Checksum mismatch [data:\(chksum) vs calculated:\(self.checksum)]")
        }
    }
    
    
    init(cmd: Command) {
        type = .Command
        content = cmd
        length = cmd.serialize().length
    }
    
    init(resp: Response) {
        type = .Response
        content = resp
        length = resp.serialize().length
    }

    init(update: Update) {
        type = .Update
        content = update
        length = update.serialize().length
    }
    
    func serialize() -> NSData {
        //Assumes checksum, length, type are already set
        let data = NSMutableData(length: length + 2) //2 for checksum and type byte
        var rawType : UInt8 = type.rawValue
        if let data = data {
            data.replaceBytesInRange(NSMakeRange(typeIndex, sizeof(UInt8)), withBytes: &rawType)
            data.replaceBytesInRange(NSMakeRange(lengthIndex, sizeof(UInt8)), withBytes: &length)
            data.replaceBytesInRange(NSMakeRange(contentIndex + length, sizeof(UInt8)), withBytes: &checksum)
            
            if let command = content as? Command {
                data.replaceBytesInRange(NSMakeRange(contentIndex, length), withBytes: command.serialize().bytes)
            } else if let response = content as? Response {
                data.replaceBytesInRange(NSMakeRange(contentIndex, length), withBytes: response.serialize().bytes)
            } else if let update = content as? Update {
                data.replaceBytesInRange(NSMakeRange(contentIndex, length), withBytes: update.serialize().bytes)
            }
        }

        if let data = data {
            return NSData(data: data)
        } else {
            return NSData()
        }
    }

    var description: String {
        let me = String(self.dynamicType).componentsSeparatedByString(".").last!
        return "\(me)::\(content)"
    }

}
