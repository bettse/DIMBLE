//
//  ProxyBase.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/14/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation

class ProxyBase {
    
    let fakeBase = FakeBaseInterface()
    let realBase = RealBase()
    
    init() {
        realBase.registerBaseFoundCallback(baseFound)
        fakeBase.registerIncomingReportCallback(fromGame)
        realBase.registerIncomingReportCallback(fromBase)
    }
    
    func discoverRealBase() {
        print("Starting real base discovery")
        realBase.discover()
    }
    
    func baseFound() {
        print("Real base found, starting fake base")
        fakeBase.start()
    }
    
    func registerFromBaseCallback(callback: RealBase.incomingReport) {
        realBase.registerIncomingReportCallback(callback)
    }
    
    func registerFromGameCallback(callback: FakeBaseInterface.incomingReport) {
        fakeBase.registerIncomingReportCallback(callback)
    }
    
    func fromBase(report: Report) {
        fakeBase.outgoingReport(report)
    }

    func fromGame(report: Report) {
        realBase.outgoingReport(report)
    }
}