//
//  TheLibrary.swift
//  DIMBLE
//
//  Created by Eric Betts on 11/28/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
import UIKit

class TheLibrary {
    static let singleton : TheLibrary = TheLibrary()
    let queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)

    let fileManager = NSFileManager()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var fileList : [EncryptedToken] = []
 
    init() {
        dispatch_async(queue, {
            self.fileList = self.loadFileList()
            print("TheLibrary has loaded \(self.fileList.count) files")
        })
    }
    
    func loadFileList() -> [EncryptedToken] {
        var fileList = [EncryptedToken]()

        let files = self.fileManager.enumeratorAtURL(
            self.appDelegate.applicationDocumentsDirectory,
            includingPropertiesForKeys: nil,
            options: NSDirectoryEnumerationOptions.SkipsHiddenFiles,
            errorHandler: nil)
        
        if let files = files {
            for file in files {
                if file.absoluteString.hasSuffix("bin") { // checks the extension
                    if let image = NSData(contentsOfURL: file as! NSURL) {
                        if (image.length == MifareMini.tokenSize) {
                            fileList.append(EncryptedToken(image: image))
                        }
                    }
                }
            }
        }
        
        
        fileList.sortInPlace({ (a, b) -> Bool in
            //Needs to be able to sort figures, playsets, etc
            return a.decryptedToken.modelId > b.decryptedToken.modelId
        })
        return fileList
    }
    
    func forPlatform(platform: LedPlatform) -> [EncryptedToken] {
        return fileList.filter { (encryptedToken) -> Bool in
            let token : Token = encryptedToken.decryptedToken
            var validShape : [Model.Shape] = []
            if (platform == .Right || platform == .Left) {
                validShape = [Model.Shape.Figure, Model.Shape.RoundPowerDisk]
            } else if (platform == .Hex) {
                validShape = [Model.Shape.HexPowerDisk, Model.Shape.PlaySet]
            }
            
            return validShape.contains(token.model.shape)
        }
    }

}